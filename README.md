# README

## Setup

```
bundle install
rails db:seed
```

## Run

`rails server`

Navigate to `http://localhost:3000/postcode`

## Test

`rspec`

## Notes

As there's no admin for configuring approved postcodes / editing LSOAs, there are no validations on the models. This would be added before any code that could edit these.