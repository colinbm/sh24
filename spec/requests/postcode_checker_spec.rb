# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'PostcodeCheckers', type: :request do
  describe 'checking an approved postcode' do
    it 'returns valid' do
      post '/postcode', params: { postcode: { postcode: 'SE17QD' } }, xhr: true
      expect(response.body).to match(/SE17QD is allowed/)
    end
  end

  describe 'checking an unapproved postcode' do
    it 'returns invalid' do
      post '/postcode', params: { postcode: { postcode: 'OUTSIDE' } }, xhr: true
      expect(response.body).to match(/OUTSIDE is not allowed/)
    end
  end

  describe 'checking an invalid postcode' do
    it 'returns invalid' do
      post '/postcode', params: { postcode: { postcode: 'INVALID' } }, xhr: true
      expect(response.body).to match(/INVALID is not allowed/)
    end
  end

  describe 'checking a blank postcode' do
    it 'returns invalid' do
      post '/postcode', params: { postcode: { postcode: '' } }, xhr: true
      expect(response.body).to match(/ is not allowed/)
    end
  end
end
