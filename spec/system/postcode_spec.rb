# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Postcode checker', type: :system do
  before(:all) do
    WebMock.allow_net_connect!
  end

  after(:all) do
    WebMock.disable_net_connect!
  end

  it 'submits the postcode form' do
    visit '/postcode'

    expect(page).to have_content('Postcode:')

    fill_in 'postcode_postcode', with: 'SE1 7QD'
    click_button 'Search'

    expect(page).to have_content('SE1 7QD is allowed')
  end
end
