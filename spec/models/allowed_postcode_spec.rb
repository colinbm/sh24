# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable Metrics/BlockLength
RSpec.describe AllowedPostcode, type: :model do
  describe '#allowed?' do
    it('allows exact postcode') do
      expect(AllowedPostcode.allowed?('AB12CD')).to be true
    end

    it('allows formatted postcode') do
      expect(AllowedPostcode.allowed?('AB1 2CD')).to be true
    end

    it('allows downcase postcode') do
      expect(AllowedPostcode.allowed?('ab12cd')).to be true
    end

    it('allows compacted remote postcode') do
      expect(AllowedPostcode.allowed?('SE17QD')).to be true
    end

    it('allows formatted remote postcode') do
      expect(AllowedPostcode.allowed?('SE1 7QD')).to be true
    end

    it('allows downcase remote postcode') do
      expect(AllowedPostcode.allowed?('se17qd')).to be true
    end

    it('does not allow "outside of area" postcode') do
      expect(AllowedPostcode.allowed?('outside')).to be false
    end

    it('does not allow invalid postcode') do
      expect(AllowedPostcode.allowed?('invalid')).to be false
    end
  end
end
# rubocop:enable Metrics/BlockLength
