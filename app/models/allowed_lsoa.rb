# frozen_string_literal: true

class AllowedLsoa < ApplicationRecord
  def self.regex
    Regexp.new("^(?:#{all.pluck(:lsoa).join('|')}) ")
  end
end
