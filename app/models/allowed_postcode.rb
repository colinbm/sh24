# frozen_string_literal: true

class AllowedPostcode < ApplicationRecord
  def self.allowed?(postcode)
    postcode = postcode.sub(' ', '').upcase
    return true if find_by(postcode:)

    pio = Postcodes::IO.new
    result = pio.lookup(postcode)
    result && (result.lsoa =~ AllowedLsoa.regex) ? true : false
  end
end
