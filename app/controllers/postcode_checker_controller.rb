# frozen_string_literal: true

class PostcodeCheckerController < ApplicationController
  def index; end

  def result
    @postcode = postcode_params[:postcode]
    @allowed = !@postcode.blank? && AllowedPostcode.allowed?(@postcode)
  end

  private

  def postcode_params
    params.require(:postcode)
  end
end
